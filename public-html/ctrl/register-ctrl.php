<?php
 
 ini_set("display_errors",1);

 session_start();

    
    if(isset($_POST["email"]) && isset($_POST["password"]) ){                       // Si les variables $_POST existeixen

        $email = $_POST["email"];                                                   // Saneja i assigna params
        $confirmEmail = $_POST["confirmEmail"];
        $pass = $_POST["password"];
        $confirmPass = $_POST["confirmPassword"];
       
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);
        $confirmEmail = filter_var($confirmEmail, FILTER_SANITIZE_EMAIL);
        
     
        if ($email == $confirmEmail && $pass== $confirmPass) {                      // Si els camps de confirmació són equivalents al respectiu input
      

                $db = new PDO('mysql:host=db;dbname=DB;port=3306', 'user', '1234'); // Crea una connexió amb la base de dades


                $query = "INSERT INTO user (name, password) VALUES (:email, :pass)"; // Assigna la següent String a la var query (sentència a executar)
                $statement = $db->prepare($query);                                   // Preparació de la sentència per ser executada
                $statement->bindParam(":email", $email, PDO::PARAM_STR);             // Assignar params vinculant amb tipus
                $statement->bindParam(":pass", $pass, PDO::PARAM_STR);               // Assignar params vinculant amb tipus

                $result= $statement->execute();                                      // Guarda el resultat de la sentència executada (per debugar, es pot treure)
                
                $message = '<i>Usuari afegit</i>';                                   // Missatge de confirmació a mostrar a la vista
                include("../view/register.php");                                     // Inclou vista

                

        
        } else {                                                                     // Si els camps de confirmació NO són equivalents

            $message = '<i>No es pot afegir usuari: Les dades no coincideixen</i>'; 
           
            include("../view/register.php");

            die();

        }
  
    } else {                                                                                        // Si les variables POST són buides
        
        include("../view/register.php");



    }






?>