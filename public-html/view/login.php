<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Simple Login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="../resources/login.css">  
</head>
  <body>

   
  <?php 
   
    if(isset($message)){ 
      echo "<div class=\"alert alert-dark\" role=\"alert\">"; 
      echo $message; 
      echo "</div>";  
      $message="";
    } 
   ?>

<?php    
//   if(isset($_GET['error'])){ 
//    echo "<div class=\"alert alert-dark\" role=\"alert\">"; 
//     echo $_GET['error'];
//     echo "</div>";  
// } 
  ?>
   
     <section id="main">
        <h1> Simple Login </h1>
        <form action="../ctrl/login-ctrl.php" method="post">
                <!-- Email input -->
                <div class="form-outline mb-4">
                    <input name="email" type="email" id="input-email" class="form-control" />
                    <label class="form-label" for="input-email">Email address</label>
                </div>

                <!-- Password input -->
                <div class="form-outline mb-4">
                    <input name="password" type="password" id="input-password" class="form-control" />
                    <label class="form-label" for="input-password">Password</label>
                </div>

                <!-- 2 column grid layout for inline styling -->
                <div class="row mb-4">
                    <div class="col d-flex justify-content-center">
                    <!-- Checkbox -->
                    <div class="form-check">
                        <input name="input-checkbox" class="form-check-input" type="checkbox" value="" id="input-checkbox" checked />
                        <label class="form-check-label" for="input-checkbox"> Remember me </label>
                    </div>
                    </div>

                    <div class="col">
                    <!-- Simple link -->
                    <a href="#!">Forgot password?</a>
                    </div>
                </div>

                <!-- Submit button -->
                <button type="submit" class="btn btn-primary btn-block mb-4">Sign in</button>

                <!-- Register buttons -->
                <div class="text-center">
                    <p>Not a member? <a href="../ctrl/register-ctrl.php">Register</a></p>
                </div>
        </form>
     </section>




    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
  </body>
</html>
